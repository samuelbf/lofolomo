# Send all commands run locally to systemd-journal (via syslog)
# Source: https://www.octopuce.fr/suivi-des-commandes-saisies-sur-nos-serveurs/ [fr]

function trap_to_journal() { logger -p 7 -t shell-interactive "${USER}@${HOSTNAME}:${PWD}> ${BASH_COMMAND}"
}

trap trap_to_journal DEBUG

# Lofolomo

**Lo**w **fo**otprint **lo**g **mo**nitoring

## The concept

- (if applicable) keep your configuration in a gitlab project (flat files, ansible, puppet, terraform, ...)
- (if you have several systems) centralize your logs with systemd-journal-([upload](https://www.freedesktop.org/software/systemd/man/systemd-journal-upload.html)|[remote](https://www.freedesktop.org/software/systemd/man/systemd-journal-remote.html))
- make them easily searchable with [systemd-journal-gatewayd](https://www.freedesktop.org/software/systemd/man/systemd-journal-gatewayd.html) (a [new version](https://github.com/systemd/systemd/pull/17314) may be available soon)
- send them to [prometheus alertmanager](https://www.prometheus.io/docs/alerting/latest/alertmanager/), e.g. with [this prototype](https://gitlab.com/samuelbf/journal-to-alertmanager) (PROTOTYPE ! DO NOT USE FOR REAL STUFF)
- filter out noise and send alerts to gitlab : escalate manually or automatically to incidents (issues), using gitlab as a ticketing system

The code here demonstrates this idea : [watch the logs](https://lofolomo.ovh/journal/) (username : demo / password : lofolomo) or [browse incidents](https://gitlab.com/samuelbf/lofolomo/-/incidents).


```
    +---------------------------+-------------------------------+
    |                        GITLAB                             |
    |                           |                               |
    |    configuration    <--- merge request ---- issues        |
    |       as code             |                incidents      |
    |                           |                               |
    |   |                       |                   ^           | 
    +---|-----------------------+-------------------|-----------+
        |                                           |
    configures                                    alerts
        |                                           |
        |       +---------------------------+       |
        +--------->     LOG MACHINE         |       |
        |       |                           |       |
        |       |           alertmanager -----------+           +----
        |       |                           |                   | Journal web GUI
        |       | systemd-journal-gatewayd ------logs---------->|
        |       |                           |                   |  =====
        |       | systemd-journal-remote <------+               |  =========
        |       +---------------------------+   |               |  ========
        |                                       |               |  ================
        |       +---------------------------+  logs             |  ============
        +--------->      MACHINE A          |   |               |  =====
        |       |                           |   |               |
        |       |  systemd-journal-upload ------+               +----
        |       +---------------------------+   |
        |                                       |
        |       +---------------------------+   |
        +--------->      MACHINE B          |   |
        |       |                           |   |
        |       |  systemd-journal-upload ------+
        |       +---------------------------+   |
        |                                       |
        ...                                    ...
        
```

In short, we're putting systemd logs in the middle of a devOps loop.

## Benefits

### Auditable knowledge about your systems

Logs will generate alerts which will materialize as issues on gitlab (assuming you auto-escalate alerts to incidents). You now have the space to document what happened (in human language) & the severity of the event and its consequences. You will then probably open au merge request either to fix the problem or to ignore alerts by adding a rule to [alertmanager.yml](roles/journal-alert/templates/prometheus-alertmanager.yml.j2).

What you get from this ? 
- extensive information about your system
- summarized into alertmanager.yml on one side (ignore rules) and your configuration on the other side (problems fixed)
- both are easily auditable : `git blame` or [gitlab blame](https://gitlab.com/samuelbf/lofolomo/-/blame/master/roles/journal-alert/templates/prometheus-alertmanager.yml.j2) links back to commits, merge request, discussion and incidents that motivated the change.

You're fixing things and building documentation in the same time.

### Reviewable ops

We review code. But real data and real risks are on the ops side : an administrator fixing things on a machine may make mistakes and these mistakes may have immediate consequences. Security-minded teams should review every manual operation on production systems.

The architecture presented here equips you for reviewing ops :

- the log journal make it easy to find "who did what" on the machine
- you can raise alerts on logins and/or on administrative operations (sudo)
- this alert generate an issue where the administrator can explain what he did and someone else can review logs, confirm, and close the issue.

Note that it brings a solution for a uncommon but serious issue : the rogue sysadmin case. Now, you can detect it.

### Earlier failure detection

There's 3 types of monitoring :

- **Service monitoring** (nagios, icinga, centreon, ...) : they poll your service regurlarly and raise alerts when they detect the service is down.
- **Log monitoring** (ELK, checklog, this project, ...) : they raise alerts when your application emits unexpected or failure messages. You get the alert earlier than with service monitoring, but you will miss some problems (silent crash, network failure...)
- **Metrics** (prometheus, TICK stack, ...) : they raise alerts based on metrics and are very handy for monitoring resources. You get alerts even earlier (e.g. when your system starts getting too low on RAM or too high on CPU), but only for resource-related failures.

You need all 3.

## Status of this project

This is a demo for a prototype. Lot of work needs to be done in order to provide a real solution for real sysadmins : 

- I tricked alertmanager to do log filtering, it's currently not a thing the authors would expect. I'm not sure it's a very good tool for that, especially since [grouping alerts does not work for gitlab](https://gitlab.com/samuelbf/lofolomo/-/issues/1). Maybe we should develop a new one from scratch, inspired by [nftables](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page).
- we need a proper way to send events from systemd-journal to alertmanager
- gitlab alert interface could be even more useful with filters by environment or hosts. And we miss the ability to conditionnally escalate to incidents (e.g escalate for prod and preprod environments, but not for dev or test environments...).

Nobody is working on it for now.
